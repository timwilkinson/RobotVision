# RobotVision
MJPEG video HTTP streamer for Raspberry Pi

This is a simple MJPEG HTTP video streamer original written to run on the Raspberry Pi. The video input is handled using OpenCV,
and the output server based on this web server (https://github.com/eidheim/Simple-Web-Server).

The choice of OpenCV is overkill for a simple video server, but this project is the basis for other projects which will
be performing various image recognition tasks, so it makes sense to capture the video directly with OpenCV from the start.
